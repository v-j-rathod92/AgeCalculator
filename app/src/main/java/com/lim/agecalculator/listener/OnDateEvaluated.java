package com.lim.agecalculator.listener;

/**
 * Created by rgi-40 on 31/1/18.
 */

public interface OnDateEvaluated {
    public void dateEvaluated(String day);
}
