package com.lim.agecalculator;

import android.app.Application;

import net.danlew.android.joda.JodaTimeAndroid;

/**
 * Created by rgi-40 on 5/2/18.
 */

public class AgeCalculator extends Application{
    @Override
    public void onCreate() {
        super.onCreate();
        JodaTimeAndroid.init(this);
    }
}
