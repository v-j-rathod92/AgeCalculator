package com.lim.agecalculator;

import android.app.Activity;
import android.view.inputmethod.InputMethodManager;

import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.format.DateTimeFormat;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by rgi-40 on 1/2/18.
 */

public class Utils {
    public static void hideSoftKeyBoard(Activity activity) {
        final InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager.isActive()) {
            if (activity.getCurrentFocus() != null) {
                inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
            }
        }
    }


    public static String getDayOfWeek() {
        Calendar calendar = Calendar.getInstance();
        return getDayOfWeek(calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.YEAR));
    }

    public static String getDayOfWeek(int day, int month, int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.YEAR, year);

        Date date = new Date();
        date.setTime(calendar.getTimeInMillis());

        LocalDate localDate = new LocalDate(date);
        return DateTimeFormat.forPattern("EEEE").print(localDate);
    }

    public static int getCurrentDay() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.DAY_OF_MONTH);
    }

    public static int getCurrentMonth() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.MONTH);
    }

    public static int getCurrentYear() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.YEAR);
    }

    public static int[] calculateDifferent(int fromDD, int fromMM, int fromYYYY,
                                           int toDD, int toMM, int toYYYY) {
        int[] age = new int[3];
        Date dateFrom = prepareDate(fromDD, fromMM, fromYYYY);
        Date dateTo = prepareDate(toDD, toMM, toYYYY);

        LocalDate fromDate = new LocalDate(dateFrom);
        LocalDate toDate = new LocalDate(dateTo);

        Period period = new Period(fromDate, toDate, PeriodType.yearMonthDay());
        age[2] = period.getDays();
        age[1] = period.getMonths();
        age[0] = period.getYears();
        return age;
    }

    public static int[] calculateComingBirthday(int dd, int mm, int yyyy) {
        int[] age = new int[2];
        Date dateBirthday = prepareDate(dd, mm, yyyy);

        LocalDate localDateBirthday = new LocalDate(dateBirthday);
        LocalDate localDateToday = LocalDate.now();

        Period period = new Period(localDateToday, localDateBirthday, PeriodType.yearMonthDay());
        age[1] = period.getMonths();
        age[0] = period.getDays();
        return age;
    }

    public static Date prepareDate(int dd, int mm, int yyyy) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, dd);
        calendar.set(Calendar.MONTH, mm - 1);
        calendar.set(Calendar.YEAR, yyyy);

        Date date = new Date();
        date.setTime(calendar.getTimeInMillis());

        return date;
    }

    public static int[] calculateExtraInformation(int fromDD, int fromMM, int fromYYYY,
                                                  int toDD, int toMM, int toYYYY) {
        int[] info = new int[7];
        Date dateFrom = prepareDate(fromDD, fromMM, fromYYYY);
        Date dateTo = prepareDate(toDD, toMM, toYYYY);

        LocalDate fromDate = new LocalDate(dateFrom);
        LocalDate toDate = new LocalDate(dateTo);

        Period periodYears = new Period(fromDate, toDate, PeriodType.years());
        info[0] = periodYears.getYears();

        Period periodMonths = new Period(fromDate, toDate, PeriodType.months());
        info[1] = periodMonths.getMonths();

        Period periodWeeks = new Period(fromDate, toDate, PeriodType.weeks());
        info[2] = periodWeeks.getWeeks();

        Period periodDays = new Period(fromDate, toDate, PeriodType.days());
        info[3] = periodDays.getDays();

        Period periodHours = new Period(fromDate, toDate, PeriodType.hours());
        info[4] = periodHours.getHours();

        Period periodMinutes = new Period(fromDate, toDate, PeriodType.minutes());
        info[5] = periodMinutes.getMinutes();

        Period periodSeconds = new Period(fromDate, toDate, PeriodType.seconds());
        info[6] = periodSeconds.getSeconds();

        return info;
    }

    public static String getFormatedDate(int dd, int mm, int yyyy) {
        Date date = prepareDate(dd, mm, yyyy);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
        return dateFormat.format(date);
    }
}

