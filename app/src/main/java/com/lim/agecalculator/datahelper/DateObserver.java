package com.lim.agecalculator.datahelper;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.lim.agecalculator.Utils;
import com.lim.agecalculator.listener.OnDateEvaluated;

/**
 * Created by rgi-40 on 31/1/18.
 */

public class DateObserver implements TextWatcher {
    private OnDateEvaluated onDateEvaluated;
    private EditText edtDD, edtMM, edtYYYY;
    private Context mContext;

    public void observeDate(Context mContext, EditText edtDD, EditText edtMM, EditText edtYYYY,
                            OnDateEvaluated onDateEvaluated) {
        this.mContext = mContext;
        this.onDateEvaluated = onDateEvaluated;

        this.edtDD = edtDD;
        this.edtMM = edtMM;
        this.edtYYYY = edtYYYY;

        this.edtDD.addTextChangedListener(this);
        this.edtMM.addTextChangedListener(this);
        this.edtYYYY.addTextChangedListener(this);
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        onDateEvaluated.dateEvaluated(getFromDateDay());
    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (editable != null && !editable.toString().equalsIgnoreCase("")) {
            if (edtDD.getText().hashCode() == editable.hashCode()) {
                if (edtDD.getText().toString().length() == 2) {
                    edtMM.requestFocus();
                }
            } else if (edtMM.getText().hashCode() == editable.hashCode()) {
                if (edtMM.getText().toString().length() == 2) {
                    edtYYYY.requestFocus();
                }

            } else if (edtYYYY.getText().hashCode() == editable.hashCode()) {
                if (edtYYYY.getText().toString().length() == 4) {
                    Utils.hideSoftKeyBoard((Activity) mContext);
                }
            }
        }
    }

    private String getFromDateDay() {
        String dayOfWeek = "";
        if (validateFromDate()) {
            dayOfWeek = Utils.getDayOfWeek(Integer.parseInt(edtDD.getText().toString()),
                    Integer.parseInt(edtMM.getText().toString()) - 1,
                    Integer.parseInt(edtYYYY.getText().toString()));
        }
        return dayOfWeek;
    }

    private boolean validateFromDate() {
        if (edtDD.getText().toString().length() != 0 && edtMM.getText().toString().length() != 0
                && edtYYYY.getText().toString().length() == 4) {
            return true;
        } else {
            return false;
        }
    }
}
