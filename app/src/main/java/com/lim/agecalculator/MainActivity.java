package com.lim.agecalculator;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.lim.agecalculator.datahelper.DateObserver;
import com.lim.agecalculator.listener.OnDateEvaluated;

import org.joda.time.LocalDate;

import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText edtFromDD, edtFromMM, edtFromYYYY;
    private EditText edtToDD, edtToMM, edtToYYYY;
    private TextView txtFromDay, txtToDay;
    private TextView txtCalculate, txtClear;
    private TextView txtTotalYears, txtTotalMonths, txtTotalDays;
    private TextView txtNextBirthdayMonths, txtNextBirthdayDays;
    private TextView txtYears, txtMonths, txtWeeks, txtDays, txtHours, txtMinutes, txtSeconds;
    private TextView txtUpcomingBirthdayLabel;
    private LinearLayout linearLayoutUpcomingBirthday;
    private TextView txtNextComingBirthdayYear1, txtNextComingBirthdayYear2, txtNextComingBirthdayYear3, txtNextComingBirthdayYear4,
            txtNextComingBirthdayYear5, txtNextComingBirthdayYear6, txtNextComingBirthdayYear7, txtNextComingBirthdayYear8,
            txtNextComingBirthdayYear9, txtNextComingBirthdayYear10;
    private TextView txtNextComingBirthday1, txtNextComingBirthday2, txtNextComingBirthday3, txtNextComingBirthday4,
            txtNextComingBirthday5, txtNextComingBirthday6, txtNextComingBirthday7, txtNextComingBirthday8,
            txtNextComingBirthday9, txtNextComingBirthday10;
    private ImageView imgPickFromDate, imgPickToDate;

    private AdView adView;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
        setListener();
    }

    private void init() {
        edtFromDD = findViewById(R.id.edtFromDD);
        edtFromMM = findViewById(R.id.edtFromMM);
        edtFromYYYY = findViewById(R.id.edtFromYYYY);
        edtToDD = findViewById(R.id.edtToDD);
        edtToMM = findViewById(R.id.edtToMM);
        edtToYYYY = findViewById(R.id.edtToYYYY);
        txtFromDay = findViewById(R.id.txtFromDay);
        txtToDay = findViewById(R.id.txtToDay);
        txtCalculate = findViewById(R.id.txtCalculate);
        txtClear = findViewById(R.id.txtClear);
        txtTotalYears = findViewById(R.id.txtTotalYears);
        txtTotalMonths = findViewById(R.id.txtTotalMonths);
        txtTotalDays = findViewById(R.id.txtTotalDays);
        txtNextBirthdayMonths = findViewById(R.id.txtNextBirthdayMonths);
        txtNextBirthdayDays = findViewById(R.id.txtNextBirthdayDays);
        txtYears = findViewById(R.id.txtYears);
        txtMonths = findViewById(R.id.txtMonths);
        txtWeeks = findViewById(R.id.txtWeeks);
        txtDays = findViewById(R.id.txtDays);
        txtHours = findViewById(R.id.txtHours);
        txtMinutes = findViewById(R.id.txtMinutes);
        txtSeconds = findViewById(R.id.txtSeconds);
        txtUpcomingBirthdayLabel = findViewById(R.id.txtUpcomingBirthdayLabel);
        linearLayoutUpcomingBirthday = findViewById(R.id.linearLayoutUpcomingBirthday);
        txtNextComingBirthdayYear1 = findViewById(R.id.txtNextComingBirthdayYear1);
        txtNextComingBirthdayYear2 = findViewById(R.id.txtNextComingBirthdayYear2);
        txtNextComingBirthdayYear3 = findViewById(R.id.txtNextComingBirthdayYear3);
        txtNextComingBirthdayYear4 = findViewById(R.id.txtNextComingBirthdayYear4);
        txtNextComingBirthdayYear5 = findViewById(R.id.txtNextComingBirthdayYear5);
        txtNextComingBirthdayYear6 = findViewById(R.id.txtNextComingBirthdayYear6);
        txtNextComingBirthdayYear7 = findViewById(R.id.txtNextComingBirthdayYear7);
        txtNextComingBirthdayYear8 = findViewById(R.id.txtNextComingBirthdayYear8);
        txtNextComingBirthdayYear9 = findViewById(R.id.txtNextComingBirthdayYear9);
        txtNextComingBirthdayYear10 = findViewById(R.id.txtNextComingBirthdayYear10);
        txtNextComingBirthday1 = findViewById(R.id.txtNextComingBirthday1);
        txtNextComingBirthday2 = findViewById(R.id.txtNextComingBirthday2);
        txtNextComingBirthday3 = findViewById(R.id.txtNextComingBirthday3);
        txtNextComingBirthday4 = findViewById(R.id.txtNextComingBirthday4);
        txtNextComingBirthday5 = findViewById(R.id.txtNextComingBirthday5);
        txtNextComingBirthday6 = findViewById(R.id.txtNextComingBirthday6);
        txtNextComingBirthday7 = findViewById(R.id.txtNextComingBirthday7);
        txtNextComingBirthday8 = findViewById(R.id.txtNextComingBirthday8);
        txtNextComingBirthday9 = findViewById(R.id.txtNextComingBirthday9);
        txtNextComingBirthday10 = findViewById(R.id.txtNextComingBirthday10);
        imgPickFromDate = findViewById(R.id.imgPickFromDate);
        imgPickToDate = findViewById(R.id.imgPickToDate);

        adView = (AdView) findViewById(R.id.adView);

        setCurrentDateAndDay();

        loadAdvertise();
    }

    private void setListener() {
        new DateObserver().observeDate(this, edtFromDD, edtFromMM, edtFromYYYY,
                new OnDateEvaluated() {
                    @Override
                    public void dateEvaluated(String day) {
                        txtFromDay.setText(day);
                    }
                });

        new DateObserver().observeDate(this, edtToDD, edtToMM, edtToYYYY,
                new OnDateEvaluated() {
                    @Override
                    public void dateEvaluated(String day) {
                        txtToDay.setText(day);
                    }
                });

        imgPickFromDate.setOnClickListener(this);
        imgPickToDate.setOnClickListener(this);
        txtCalculate.setOnClickListener(this);
        txtClear.setOnClickListener(this);

    }

    private void setCurrentDateAndDay() {
        Calendar calendar = Calendar.getInstance();
        edtFromDD.setText(String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)));
        edtFromMM.setText(String.format("%02d", calendar.get(Calendar.MONTH) + 1));
        edtFromYYYY.setText(String.valueOf(calendar.get(Calendar.YEAR)));

        txtFromDay.setText(Utils.getDayOfWeek());
    }

    private void setCurrentDateAndDay(int dd, int mm, int yyyy) {
        edtFromDD.setText(String.format("%02d", dd));
        edtFromMM.setText(String.format("%02d", mm + 1));
        edtFromYYYY.setText(String.valueOf(yyyy));

        txtFromDay.setText(Utils.getDayOfWeek(dd, mm, yyyy));
    }

    private void setToDateAndDay(int dd, int mm, int yyyy) {
        edtToDD.setText(String.format("%02d", dd));
        edtToMM.setText(String.format("%02d", mm + 1));
        edtToYYYY.setText(String.valueOf(yyyy));

        txtToDay.setText(Utils.getDayOfWeek(dd, mm, yyyy));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgPickFromDate:
                openFromDateDialog();
                break;

            case R.id.imgPickToDate:
                openToDateDialog();
                break;

            case R.id.txtCalculate:
                if (validate()) {
                    calculate();
                }
                break;

            case R.id.txtClear:
                clearData();
                break;
        }
    }

    private void openFromDateDialog() {
        DatePickerDialog fromDatePickerDialog = new DatePickerDialog(this, onFromDateSet,
                Utils.getCurrentYear(), Utils.getCurrentMonth(), Utils.getCurrentDay());
        fromDatePickerDialog.show();
    }

    private void openToDateDialog() {
        DatePickerDialog fromDatePickerDialog = new DatePickerDialog(this, onToDateSet,
                Utils.getCurrentYear(), Utils.getCurrentMonth(), Utils.getCurrentDay());
        fromDatePickerDialog.show();
    }

    DatePickerDialog.OnDateSetListener onFromDateSet = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            setCurrentDateAndDay(dayOfMonth, month, year);
            changeCursorFocus();
        }
    };

    DatePickerDialog.OnDateSetListener onToDateSet = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            setToDateAndDay(dayOfMonth, month, year);
        }
    };

    private void calculate() {
        calculateAge();
        calculateComingBirthdayTime();
        calculateExtraInformation();
        calculateComingBirthdayDays();
    }

    private void calculateAge() {
        int toDD = Integer.parseInt(edtFromDD.getText().toString());
        int toMM = Integer.parseInt(edtFromMM.getText().toString());
        int toYYYY = Integer.parseInt(edtFromYYYY.getText().toString());

        int fromDD = Integer.parseInt(edtToDD.getText().toString());
        int fromMM = Integer.parseInt(edtToMM.getText().toString());
        int fromYYYY = Integer.parseInt(edtToYYYY.getText().toString());

        int[] age = Utils.calculateDifferent(fromDD, fromMM, fromYYYY, toDD, toMM, toYYYY);

        txtTotalYears.setText(String.valueOf(age[0]));
        txtTotalMonths.setText(String.valueOf(age[1]));
        txtTotalDays.setText(String.valueOf(age[2]));
    }

    private void calculateComingBirthdayTime() {
        Calendar calendar = Calendar.getInstance();
        int[] nexBirthdate = getNextComingBirthdate(Integer.parseInt(edtToDD.getText().toString()),
                Integer.parseInt(edtToMM.getText().toString()),
                calendar.get(Calendar.YEAR));

        int[] age = Utils.calculateComingBirthday(nexBirthdate[0], nexBirthdate[1], nexBirthdate[2]);
        txtNextBirthdayMonths.setText(String.valueOf(age[1]));
        txtNextBirthdayDays.setText(String.valueOf(age[0]));
    }

    private void calculateExtraInformation() {
        int toDD = Integer.parseInt(edtFromDD.getText().toString());
        int toMM = Integer.parseInt(edtFromMM.getText().toString());
        int toYYYY = Integer.parseInt(edtFromYYYY.getText().toString());

        int fromDD = Integer.parseInt(edtToDD.getText().toString());
        int fromMM = Integer.parseInt(edtToMM.getText().toString());
        int fromYYYY = Integer.parseInt(edtToYYYY.getText().toString());

        int[] info = Utils.calculateExtraInformation(fromDD, fromMM, fromYYYY, toDD, toMM, toYYYY);

        txtYears.setText(String.valueOf(info[0]));
        txtMonths.setText(String.valueOf(info[1]));
        txtWeeks.setText(String.valueOf(info[2]));
        txtDays.setText(String.valueOf(info[3]));
        txtHours.setText(String.valueOf(info[4]));
        txtMinutes.setText(String.valueOf(info[5]));
        txtSeconds.setText(String.valueOf(info[6]));
    }

    private void calculateComingBirthdayDays() {
        txtUpcomingBirthdayLabel.setVisibility(View.VISIBLE);
        linearLayoutUpcomingBirthday.setVisibility(View.VISIBLE);

        Calendar calendar = Calendar.getInstance();
        int[] nexBirthdate = getNextComingBirthdate(Integer.parseInt(edtToDD.getText().toString()),
                Integer.parseInt(edtToMM.getText().toString()),
                calendar.get(Calendar.YEAR));
        int dd = nexBirthdate[0];
        int mm = nexBirthdate[1];
        int yyyy = nexBirthdate[2];
        mm = mm - 1;

        txtNextComingBirthdayYear1.setText(Utils.getFormatedDate(dd, mm, yyyy));
        txtNextComingBirthday1.setText(Utils.getDayOfWeek(dd, mm, yyyy));

        yyyy = yyyy + 1;

        txtNextComingBirthdayYear2.setText(Utils.getFormatedDate(dd, mm, yyyy));
        txtNextComingBirthday2.setText(Utils.getDayOfWeek(dd, mm, yyyy));

        yyyy = yyyy + 1;

        txtNextComingBirthdayYear3.setText(Utils.getFormatedDate(dd, mm, yyyy));
        txtNextComingBirthday3.setText(Utils.getDayOfWeek(dd, mm, yyyy));

        yyyy = yyyy + 1;

        txtNextComingBirthdayYear4.setText(Utils.getFormatedDate(dd, mm, yyyy));
        txtNextComingBirthday4.setText(Utils.getDayOfWeek(dd, mm, yyyy));

        yyyy = yyyy + 1;

        txtNextComingBirthdayYear5.setText(Utils.getFormatedDate(dd, mm, yyyy));
        txtNextComingBirthday5.setText(Utils.getDayOfWeek(dd, mm, yyyy));

        yyyy = yyyy + 1;

        txtNextComingBirthdayYear6.setText(Utils.getFormatedDate(dd, mm, yyyy));
        txtNextComingBirthday6.setText(Utils.getDayOfWeek(dd, mm, yyyy));

        yyyy = yyyy + 1;

        txtNextComingBirthdayYear7.setText(Utils.getFormatedDate(dd, mm, yyyy));
        txtNextComingBirthday7.setText(Utils.getDayOfWeek(dd, mm, yyyy));

        yyyy = yyyy + 1;

        txtNextComingBirthdayYear8.setText(Utils.getFormatedDate(dd, mm, yyyy));
        txtNextComingBirthday8.setText(Utils.getDayOfWeek(dd, mm, yyyy));

        yyyy = yyyy + 1;

        txtNextComingBirthdayYear9.setText(Utils.getFormatedDate(dd, mm, yyyy));
        txtNextComingBirthday9.setText(Utils.getDayOfWeek(dd, mm, yyyy));

        yyyy = yyyy + 1;

        txtNextComingBirthdayYear10.setText(Utils.getFormatedDate(dd, mm, yyyy));
        txtNextComingBirthday10.setText(Utils.getDayOfWeek(dd, mm, yyyy));
    }

    private int[] getNextComingBirthdate(int dd, int mm, int yyyy) {
        Date dateBirthdate = Utils.prepareDate(dd, mm, yyyy);

        LocalDate localBirthdate = new LocalDate(dateBirthdate);
        LocalDate localToday = LocalDate.now();

        if (localToday.isBefore(localBirthdate)) {
            return new int[]{dd, mm, yyyy};
        } else {
            return new int[]{dd, mm, yyyy + 1};
        }
    }


    private boolean validate() {
        if (validateFromDate() && validateToDate()) {
            return true;
        } else {
            return false;
        }
    }

    private boolean validateFromDate() {
        if (edtFromDD.getText().toString().length() != 0 && edtFromMM.getText().toString().length() != 0
                && edtFromYYYY.getText().toString().length() == 4) {
            return true;
        } else {
            return false;
        }
    }

    private boolean validateToDate() {
        if (edtToDD.getText().toString().length() != 0 && edtToMM.getText().toString().length() != 0
                && edtToYYYY.getText().toString().length() == 4) {
            return true;
        } else {
            return false;
        }
    }

    private void clearData() {
        txtFromDay.setText("");
        txtTotalDays.setText("");
        setCurrentDateAndDay();
        edtToDD.setText("");
        edtToMM.setText("");
        edtToYYYY.setText("");
        txtTotalYears.setText(getString(R.string.txtDoubleZero));
        txtTotalMonths.setText(getString(R.string.txtDoubleZero));
        txtTotalDays.setText(getString(R.string.txtDoubleZero));
        txtNextBirthdayMonths.setText(getString(R.string.txtDoubleZero));
        txtNextBirthdayDays.setText(getString(R.string.txtDoubleZero));
        txtYears.setText(getString(R.string.txtZero));
        txtMonths.setText(getString(R.string.txtZero));
        txtWeeks.setText(getString(R.string.txtZero));
        txtDays.setText(getString(R.string.txtZero));
        txtHours.setText(getString(R.string.txtZero));
        txtMinutes.setText(getString(R.string.txtZero));
        txtSeconds.setText(getString(R.string.txtZero));

        txtUpcomingBirthdayLabel.setVisibility(View.GONE);
        linearLayoutUpcomingBirthday.setVisibility(View.GONE);

        edtToDD.requestFocus();
    }

    private void changeCursorFocus() {
        // if to date is selected then hide keyboard else request focus
        if (validateToDate()) {
            Utils.hideSoftKeyBoard(this);
        } else {
            edtToDD.requestFocus();
        }
    }

    private void loadAdvertise() {
        AdRequest adRequest = new AdRequest.Builder()
//                .addTestDevice("C21C8AEABCB925D5933727E23B5C2567")
                .build();

        AdListener adListener = new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                adView.setVisibility(View.VISIBLE);
            }
        };

        adView.setAdListener(adListener);
        adView.loadAd(adRequest);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.fullscreen_ad_unit_id));
        mInterstitialAd.loadAd(adRequest);
    }

    @Override
    public void onBackPressed() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            super.onBackPressed();
        }
    }
}
